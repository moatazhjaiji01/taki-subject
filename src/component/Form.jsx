import React, { useRef, useState } from "react";
import "./../scss/form.scss";
//import data from "./../data/subject";

const Form = ({ setSubjects, subjects }) => {
  const [name, setName] = useState("");
  const [desc, setDesc] = useState("");
  const [nameFalse, isNameFalse] = useState(false);
  const [descFalse, isDescFalse] = useState(false);
  const nameEmpty = useRef("");
  const descEmpty = useRef("");

  const handlerForm = (e) => {
    e.preventDefault();
    isNameFalse(false);
    isDescFalse(false);
    if (!name) {
      // nameEmpty.current.style.backgroundColor = "#fde6e6";
      isNameFalse(true);
      return;
    }
    if (!desc) {
      // descEmpty.current.style.backgroundColor = "#fde6e6";
      // descEmpty.current.style.Color = "#d15353";
      isDescFalse(true);
      return;
    }
    setName("");
    setDesc("");
    console.log("ff", subjects);
    const newSubject = {
      id: new Date().getTime(),
      name: name,
      description: desc,
    };
    console.log("first", [newSubject, ...subjects]);
    setSubjects([...subjects, newSubject]);
  };
  //useEffect(isNameFalse(true), [name]);
  return (
    <div className="form">
      <form onSubmit={handlerForm}>
        <label htmlFor="name-subject">Ajouter une matiere</label>
        <input
          type="text"
          id="name-subject"
          placeholder="Nom"
          value={name}
          ref={nameEmpty}
          onChange={(e) => setName(e.target.value)}
          className={nameFalse && "false"}
        />
        <textarea
          name="desc"
          id="desc"
          cols="30"
          rows="10"
          placeholder="Description"
          value={desc}
          ref={descEmpty}
          onChange={(e) => setDesc(e.target.value)}
          className={descFalse && "false"}
        ></textarea>
        <button type="submit">Ajouter</button>
      </form>
    </div>
  );
};

export default Form;
