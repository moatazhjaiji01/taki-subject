import React from "react";
import "./../scss/modal.scss";

const Modal = ({
  modalActive,
  idSelected,
  subjects,
  setModalActive,
  setSubjects,
}) => {
  const body = document.querySelector("body");
  if (modalActive) {
    const modal = document.querySelector(".modal");
    const topModal = window.pageYOffset;
    body.style.overflow = "hidden";
    modal.style.top = topModal + "px";
  }
  const subject = subjects.find((item) => idSelected == item.id);
  console.log(subject);
  const deleteSubject = () => {
    const newSubject = subjects.filter((subject) => {
      return subject.id !== idSelected;
    });
    setModalActive(false);
    setSubjects(newSubject);
    body.style.overflow = "initial";
  };
  // const { name, description } = subject;
  // console.log(name, description);
  return (
    <div className={`${modalActive && "active"} modal`}>
      <div className="modal-popup">
        <h1 className="modal-title">{subject?.name}</h1>
        <p className="modal-description">{subject?.description}</p>
        <div className="button">
          <button className="delete" onClick={deleteSubject}>
            Supprimer{" "}
          </button>
          <button
            className="cancel"
            onClick={() => {
              setModalActive(false);
              body.style.overflow = "initial";
            }}
          >
            Fermer
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
