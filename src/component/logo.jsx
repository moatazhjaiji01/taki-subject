import React from "react";
import "./../scss/logo.scss";

const logo = () => {
  return (
    <p className="logo">
      <span>Taki</span>
      <span>Academy</span>
    </p>
  );
};

export default logo;
