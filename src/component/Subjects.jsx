import React, { useState } from "react";
import Modal from "./../component/Modal";
import "./../scss/subject.scss";

const Subjects = ({ subjects, setModalActive, modalActive, setSubjects }) => {
  const [idSelected, setIdSelected] = useState("");
  const showModal = (id) => {
    setModalActive(true);
    setIdSelected(id);
    console.log(id);
  };
  return (
    <>
      <p className="number-matiere">Il y a {subjects.length} matieres.</p>
      <div className="subjects">
        {subjects.map((subject) => {
          const { id, name, description } = subject;
          return (
            <div
              className="subject"
              key={id}
              onClick={(e) => {
                showModal(id);
              }}
            >
              <h1 className="subject-title">{name}</h1>
              <p className="subject-description">{description}</p>
            </div>
          );
        })}
        <Modal
          subjects={subjects}
          modalActive={modalActive}
          idSelected={idSelected}
          setModalActive={setModalActive}
          setSubjects={setSubjects}
        />
      </div>
    </>
  );
};

export default Subjects;
