import { useState } from "react";
import "./App.css";
import Logo from "./component/logo.jsx";
import Subjects from "./component/Subjects";
import Form from "./component/Form";
import data from "./data/subject";
import Modal from "./component/Modal";

function App() {
  const [subjects, setSubjects] = useState(data);
  const [modalActive, setModalActive] = useState(false);
  return (
    <>
      <Logo />
      <div className="subjects-form">
        <Subjects
          subjects={subjects}
          setModalActive={setModalActive}
          modalActive={modalActive}
          setSubjects={setSubjects}
        />
        <Form setSubjects={setSubjects} subjects={subjects} />
      </div>
    </>
  );
}

export default App;
